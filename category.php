<?php

/**
 * Class Category
 */
class Category
{

    private $delimiter = '-';
    private $filename = 'data.txt';

    /**
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * @param $delimiter
     * @return $this
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param $filename
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return array|bool
     */
    private function getCategory()
    {
        $a_data = file($this->getFilename(), FILE_IGNORE_NEW_LINES);

        if (!$a_data)
            return false;

        $a_category = [];
        foreach ($a_data as $category) {
            if (empty($category))
                continue;

            $a_scheme = explode('|', $category);
            $a_category[] = [
                'node_id' => $a_scheme[0],
                'parent_id' => $a_scheme[1],
                'node_name' => $a_scheme[2]
            ];
        }

        return $a_category;
    }

    /**
     * @return array|bool
     */
    private function getStructure()
    {
        if (!$a_category_list = $this->getCategory())
            return false;

        $a_structures = $structures = [];
        foreach ($a_category_list as $a_category) {
            $structures[$a_category['node_id']] = [
                'model' => $a_category
            ];
            if ($a_category['parent_id']) {
                if (empty($structures[$a_category['parent_id']]['children'])) {
                    $structures[$a_category['parent_id']]['children'] = [];
                }
                $structures[$a_category['parent_id']]['children'][] = &$structures[$a_category['node_id']];
            } else {
                $a_structures[] = &$structures[$a_category['node_id']];
            }
        }

        return $a_structures;
    }

    /**
     * @param null $a_structure_list
     * @param int $level
     * @return bool|string
     */
    public function getData($a_structure_list = null, $level = 0)
    {
        $catalog = '';

        if (is_null($a_structure_list) && !$a_structure_list = $this->getStructure())
            return false;

        foreach ($a_structure_list as $a_structure) {
            $delimiter = str_repeat($this->getDelimiter(), $level);
            $catalog .= $delimiter . $a_structure['model']['node_name'] . PHP_EOL;

            if (isset($a_structure['children']))
                $catalog .= $this->getData($a_structure['children'], $level + 1);
        }

        return $catalog;
    }

}

$catalog = (new Category())->setDelimiter("•")->getData();

echo '<pre>';
echo $catalog;



